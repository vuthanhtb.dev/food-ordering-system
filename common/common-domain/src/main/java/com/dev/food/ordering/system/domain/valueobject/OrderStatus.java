package com.dev.food.ordering.system.domain.valueobject;

public enum OrderStatus {
    PENDING,
    PAID,
    APPROVED,
    CANCELLING,
    CANCELLED
}
