package com.dev.food.ordering.system.domain.valueobject;

public enum OrderApprovalStatus {
    APPROVED, REJECTED
}
