package com.dev.food.ordering.system.domain;

public class DomainConstants {
    public static final String UTC = "UTC";

    private DomainConstants() {
    }
}
