package com.dev.food.ordering.system.domain.valueobject;

public enum PaymentOrderStatus {
    PENDING, CANCELLED
}
