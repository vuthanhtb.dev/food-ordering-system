package com.dev.food.ordering.system.order.service.dataaccess.customer.adapter;

import com.dev.food.ordering.system.order.service.dataaccess.customer.mapper.CustomerDataAccessMapper;
import com.dev.food.ordering.system.order.service.dataaccess.customer.repository.CustomerJpaRepository;
import com.dev.food.ordering.system.order.service.domain.entity.Customer;
import com.dev.food.ordering.system.order.service.domain.ports.output.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Component
@AllArgsConstructor
public class CustomerRepositoryImpl implements CustomerRepository {
    private final CustomerJpaRepository customerJpaRepository;
    private final CustomerDataAccessMapper customerDataAccessMapper;

    @Override
    public Optional<Customer> findCustomer(UUID customerId) {
        return customerJpaRepository.findById(customerId).map(customerDataAccessMapper::customerEntityToCustomer);
    }

    @Transactional
    @Override
    public Customer save(Customer customer) {
        return customerDataAccessMapper.customerEntityToCustomer(customerJpaRepository.save(customerDataAccessMapper.customerToCustomerEntity(customer)));
    }
}
