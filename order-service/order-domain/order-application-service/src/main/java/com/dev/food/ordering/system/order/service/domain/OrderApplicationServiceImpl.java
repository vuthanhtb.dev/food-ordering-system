package com.dev.food.ordering.system.order.service.domain;

import com.dev.food.ordering.system.order.service.domain.dto.create.CreateOrderCommand;
import com.dev.food.ordering.system.order.service.domain.dto.create.CreateOrderResponse;
import com.dev.food.ordering.system.order.service.domain.dto.track.TrackOrderQuery;
import com.dev.food.ordering.system.order.service.domain.dto.track.TrackOrderResponse;
import com.dev.food.ordering.system.order.service.domain.handler.OrderCreateCommandHandler;
import com.dev.food.ordering.system.order.service.domain.handler.OrderTrackCommandHandler;
import com.dev.food.ordering.system.order.service.domain.ports.input.service.OrderApplicationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Slf4j
@Validated
@Service
@AllArgsConstructor
class OrderApplicationServiceImpl implements OrderApplicationService {
    private final OrderCreateCommandHandler orderCreateCommandHandler;

    private final OrderTrackCommandHandler orderTrackCommandHandler;

    @Override
    public CreateOrderResponse createOrder(CreateOrderCommand createOrderCommand) {
        return orderCreateCommandHandler.createOrder(createOrderCommand);
    }

    @Override
    public TrackOrderResponse trackOrder(TrackOrderQuery trackOrderQuery) {
        return orderTrackCommandHandler.trackOrder(trackOrderQuery);
    }
}
