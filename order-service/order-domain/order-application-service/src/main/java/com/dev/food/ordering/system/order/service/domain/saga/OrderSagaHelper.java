package com.dev.food.ordering.system.order.service.domain.saga;

import com.dev.food.ordering.system.domain.valueobject.OrderId;
import com.dev.food.ordering.system.domain.valueobject.OrderStatus;
import com.dev.food.ordering.system.order.service.domain.entity.Order;
import com.dev.food.ordering.system.order.service.domain.exception.OrderNotFoundException;
import com.dev.food.ordering.system.order.service.domain.ports.output.repository.OrderRepository;
import com.dev.food.ordering.system.saga.SagaStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@AllArgsConstructor
public class OrderSagaHelper {
    private final OrderRepository orderRepository;

    Order findOrder(String orderId) {
        Optional<Order> orderResponse = orderRepository.findById(new OrderId(UUID.fromString(orderId)));
        if (orderResponse.isEmpty()) {
            log.error("Order with id: {} could not be found!", orderId);
            throw new OrderNotFoundException(String.format("Order with id %s could not be found!", orderId));
        }
        return orderResponse.get();
    }

    void saveOrder(Order order) {
        orderRepository.save(order);
    }

    public SagaStatus orderStatusToSagaStatus(OrderStatus orderStatus) {
        switch (orderStatus) {
            case PAID:
                return SagaStatus.PROCESSING;
            case APPROVED:
                return SagaStatus.SUCCEEDED;
            case CANCELLING:
                return SagaStatus.COMPENSATING;
            case CANCELLED:
                return SagaStatus.COMPENSATED;
            default:
                return SagaStatus.STARTED;
        }
    }
}
