package com.dev.food.ordering.system.order.service.domain;

import com.dev.food.ordering.system.order.service.domain.dto.message.CustomerModel;
import com.dev.food.ordering.system.order.service.domain.entity.Customer;
import com.dev.food.ordering.system.order.service.domain.exception.OrderDomainException;
import com.dev.food.ordering.system.order.service.domain.mapper.OrderDataMapper;
import com.dev.food.ordering.system.order.service.domain.ports.input.message.listener.customer.CustomerMessageListener;
import com.dev.food.ordering.system.order.service.domain.ports.output.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class CustomerMessageListenerImpl implements CustomerMessageListener {
    private final CustomerRepository customerRepository;
    private final OrderDataMapper orderDataMapper;

    @Override
    public void customerCreated(CustomerModel customerModel) {
        Customer customer = customerRepository.save(orderDataMapper.customerModelToCustomer(customerModel));

        if (null == customer) {
            log.error("Customer could not be created in order database with id: {}", customerModel.getId());
            throw new OrderDomainException(String.format("Customer could not be created in order database with id: %s", customerModel.getId()));
        }

        log.info("Customer is created in order database with id: {}", customer.getId());
    }
}
