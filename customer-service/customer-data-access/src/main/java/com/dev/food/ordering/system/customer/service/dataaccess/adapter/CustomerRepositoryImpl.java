package com.dev.food.ordering.system.customer.service.dataaccess.adapter;

import com.dev.food.ordering.system.customer.service.dataaccess.mapper.CustomerDataAccessMapper;
import com.dev.food.ordering.system.customer.service.dataaccess.repository.CustomerJpaRepository;
import com.dev.food.ordering.system.customer.service.domain.entity.Customer;
import com.dev.food.ordering.system.customer.service.domain.ports.output.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CustomerRepositoryImpl implements CustomerRepository {
    private final CustomerJpaRepository customerJpaRepository;
    private final CustomerDataAccessMapper customerDataAccessMapper;

    @Override
    public Customer createCustomer(Customer customer) {
        return customerDataAccessMapper.customerEntityToCustomer(customerJpaRepository.save(customerDataAccessMapper.customerToCustomerEntity(customer)));
    }
}
