package com.dev.food.ordering.system.customer.service.domain.ports;

import com.dev.food.ordering.system.customer.service.domain.CustomerDomainService;
import com.dev.food.ordering.system.customer.service.domain.create.CreateCustomerCommand;
import com.dev.food.ordering.system.customer.service.domain.entity.Customer;
import com.dev.food.ordering.system.customer.service.domain.event.CustomerCreatedEvent;
import com.dev.food.ordering.system.customer.service.domain.exception.CustomerDomainException;
import com.dev.food.ordering.system.customer.service.domain.mapper.CustomerDataMapper;
import com.dev.food.ordering.system.customer.service.domain.ports.output.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@AllArgsConstructor
public class CustomerCreateCommandHandler {
    private final CustomerDomainService customerDomainService;
    private final CustomerRepository customerRepository;
    private final CustomerDataMapper customerDataMapper;

    @Transactional
    public CustomerCreatedEvent createCustomer(CreateCustomerCommand createCustomerCommand) {
        Customer customer = customerDataMapper.createCustomerCommandToCustomer(createCustomerCommand);
        CustomerCreatedEvent customerCreatedEvent = customerDomainService.validateAndInitiateCustomer(customer);
        Customer savedCustomer = customerRepository.createCustomer(customer);
        if (null == savedCustomer) {
            log.error("Could not save customer with id: {}", createCustomerCommand.getCustomerId());
            throw new CustomerDomainException(String.format("Could not save customer with id: %s", createCustomerCommand.getCustomerId()));
        }

        log.info("Returning CustomerCreatedEvent for customer with id: {}", createCustomerCommand.getCustomerId());
        return customerCreatedEvent;
    }
}
