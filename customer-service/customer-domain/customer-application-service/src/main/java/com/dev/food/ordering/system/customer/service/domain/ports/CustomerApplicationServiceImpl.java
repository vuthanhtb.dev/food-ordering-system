package com.dev.food.ordering.system.customer.service.domain.ports;

import com.dev.food.ordering.system.customer.service.domain.create.CreateCustomerCommand;
import com.dev.food.ordering.system.customer.service.domain.create.CreateCustomerResponse;
import com.dev.food.ordering.system.customer.service.domain.event.CustomerCreatedEvent;
import com.dev.food.ordering.system.customer.service.domain.mapper.CustomerDataMapper;
import com.dev.food.ordering.system.customer.service.domain.ports.input.service.CustomerApplicationService;
import com.dev.food.ordering.system.customer.service.domain.ports.output.message.publisher.CustomerMessagePublisher;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Slf4j
@Validated
@Service
@AllArgsConstructor
public class CustomerApplicationServiceImpl implements CustomerApplicationService {
    private final CustomerCreateCommandHandler customerCreateCommandHandler;
    private final CustomerDataMapper customerDataMapper;
    private final CustomerMessagePublisher customerMessagePublisher;

    @Override
    public CreateCustomerResponse createCustomer(CreateCustomerCommand createCustomerCommand) {
        CustomerCreatedEvent customerCreatedEvent = customerCreateCommandHandler.createCustomer(createCustomerCommand);
        customerMessagePublisher.publish(customerCreatedEvent);
        return customerDataMapper.customerToCreatedCustomerResponse(customerCreatedEvent.getCustomer(), "Customer saved successfully");
    }
}
