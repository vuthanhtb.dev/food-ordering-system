package com.dev.food.ordering.system.customer.service.domain.mapper;

import com.dev.food.ordering.system.customer.service.domain.create.CreateCustomerCommand;
import com.dev.food.ordering.system.customer.service.domain.create.CreateCustomerResponse;
import com.dev.food.ordering.system.customer.service.domain.entity.Customer;
import com.dev.food.ordering.system.domain.valueobject.CustomerId;
import org.springframework.stereotype.Component;

@Component
public class CustomerDataMapper {
    public Customer createCustomerCommandToCustomer(CreateCustomerCommand createCustomerCommand) {
        return new Customer(new CustomerId(createCustomerCommand.getCustomerId()),
                createCustomerCommand.getUsername(),
                createCustomerCommand.getFirstName(),
                createCustomerCommand.getLastName());
    }

    public CreateCustomerResponse customerToCreatedCustomerResponse(Customer customer, String message) {
        return new CreateCustomerResponse(customer.getId().getValue(), message);
    }
}
