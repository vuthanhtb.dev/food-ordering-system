package com.dev.food.ordering.system.customer.service.domain.event;

import com.dev.food.ordering.system.customer.service.domain.entity.Customer;
import com.dev.food.ordering.system.domain.event.DomainEvent;
import lombok.AllArgsConstructor;

import java.time.ZonedDateTime;

@AllArgsConstructor
public class CustomerCreatedEvent implements DomainEvent<Customer> {
    private final Customer customer;
    private final ZonedDateTime createdAt;

    public Customer getCustomer() {
        return customer;
    }
}
