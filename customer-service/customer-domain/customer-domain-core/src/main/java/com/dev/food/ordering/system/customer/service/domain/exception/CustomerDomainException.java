package com.dev.food.ordering.system.customer.service.domain.exception;

public class CustomerDomainException extends RuntimeException {
    public CustomerDomainException(String message) {
        super(message);
    }
}
