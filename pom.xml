<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.7.9</version>
        <relativePath/>
    </parent>

    <groupId>com.dev.food.ordering.system</groupId>
    <artifactId>food-ordering-system</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>food-ordering-system</name>
    <description>Food Ordering System</description>

    <packaging>pom</packaging>

    <modules>
        <module>common</module>
        <module>infrastructure</module>
        <module>order-service</module>
        <module>customer-service</module>
        <module>payment-service</module>
        <module>restaurant-service</module>
    </modules>

    <properties>
        <java.version>17</java.version>
        <maven-compiler-plugin.version>3.9.0</maven-compiler-plugin.version>
        <spring-kafka.version>2.8.2</spring-kafka.version>
        <kafka-avro-serializer.version>7.0.1</kafka-avro-serializer.version>
        <avro.version>1.11.0</avro.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>common-domain</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>common-application</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>common-data-access</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>kafka-producer</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>kafka-consumer</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>kafka-model</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>kafka-config-data</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>saga</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>outbox</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>order-application</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>order-domain-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>order-application-service</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>order-data-access</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>order-messaging</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>payment-domain-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>payment-application-service</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>payment-data-access</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>payment-messaging</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>restaurant-domain-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>restaurant-application-service</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>restaurant-data-access</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>restaurant-messaging</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>customer-data-access</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>customer-domain-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>customer-application-service</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>customer-application</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.dev.food.ordering.system</groupId>
                <artifactId>customer-messaging</artifactId>
                <version>${project.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-logging</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <release>${java.version}</release>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
