# food-ordering-system

## Spring boot microservices with Clean & Hexagonal architectures, DDD, SAGA, Outbox, CQRS, Kafka, Kubernetes & GKE

### kafka-producer-config

`key-serializer-class` và `value-serializer-class`: cài đặt Serializer cho khóa và giá trị trong dữ liệu sẽ được ghi vào
Kafka. Trong trường hợp này, khóa dữ liệu được serialize bằng StringSerializer và giá trị dữ liệu được serialize bằng
KafkaAvroSerializer.

`compression-type`: thiết lập loại nén dữ liệu để ghi vào Kafka. Trong trường hợp này, sử dụng snappy compression.

`acks`: thiết lập số lượng Broker cần phải xử lý thành công khi Producer gửi bản ghi đến Kafka. Trong trường hợp này,
cài đặt là `all`, nghĩa là tất cả các Broker phải xử lý thành công trước khi Producer xác nhận ghi thành công và ghi dữ
liệu vào disk.

`batch-size`: thiết lập số lượng bản ghi tối đa có thể được ghi vào một partition trong một batch. Batch được gửi cho
Kafka khi số bản ghi trong batch đạt đến `batch-size`.

`batch-size-boost-factor`: yêu cầu Kafka chia sẻ kích thước bộ nhớ với batch-size của từng producer. Trong trường hợp
này, giá trị cài đặt là 100.

`linger-ms`: thiết lập một khoảng thời gian chờ trước khi ghi bản ghi vào Kafka. Trong trường hợp này, khi số bản ghi
chưa đạt đủ `batch-size`, thì producer sẽ không ghi ngay mà chờ trong khoảng thời gian 5 milliseconds.

`request-timeout-ms`: thiết lập thời gian tối đa cho một yêu cầu ghi vào Kafka. Nếu yêu cầu quá thời gian này mà không
hoàn thành, yêu cầu sẽ bị bỏ qua.

`retry-count`: thiết lập số lần Kafka Producer sẽ thử lại khi gửi yêu cầu Kafka bị lỗi. Trong trường hợp này, cài đặt là
5, vì vậy nếu gửi thông điệp gặp lỗi, nó sẽ thử lại 5 lần.

### kafka-consumer-config

`key-deserializer` và `value-deserializer`: cài đặt Deserializer cho khóa và giá trị của dữ liệu được đọc từ Kafka.
Trong trường hợp này, khóa dữ liệu được deserialize bằng StringDeserializer và giá trị dữ liệu được deserialize bằng
KafkaAvroDeserializer.

`payment-consumer-group-id` và `restaurant-approval-consumer-group-id`: định danh cho Consumer Group liên quan đến từng
chủ đề (topic) của Kafka.

`auto-offset-reset`: thiết lập khi Consumer Group bắt đầu đọc từ Kafka, nó sẽ đọc từ đầu (`earliest`) hoặc từ khoảng
thời gian mới nhất (`latest`).

`specific-avro-reader-key` và `specific-avro-reader`: sử dụng Avro Specific Record để deserialize dữ liệu Avro.

`batch-listener`: các consumer thread sẽ lắng nghe và xử lý thông điệp của Kafka theo kịch bản bằng cách đọc theo lô (
batch).

`auto-startup`: cài đặt cho consumer bắt đầu đọc dữ liệu từ Kafka ngay khi ứng dụng được khởi động.

`concurrency-level`: Số lượng consumer thread sẽ được sử dụng để đọc dữ liệu từ Kafka.

`session-timeout-ms`, `heartbeat-interval-ms` và `max-poll-interval-ms`: các thiết lập liên quan đến quá trình xác thực
và giữ kết nối giữa Kafka Consumer và Broker.

`max-poll-records`: Số lượng tối đa bản ghi (records) được phép được lấy ra từ Kafka mỗi lần yêu cầu.

`max-partition-fetch-bytes-default` và `max-partition-fetch-bytes-boost-factor`: cấu hình kích thước tối đa cho màn hình
lấy dữ liệu của Kafka. Số bytes được tải về từ mỗi khối (chunk) của một màn hình lấy tương đối nhỏ, và giá trị tăng với
max-partition-fetch-bytes-boost-factor cho đến khi N đạt được.

`poll-timeout-ms`: thời gian tối đa cho một yêu cầu nhận dữ liệu mới từ Kafka.