package com.dev.food.ordering.system.restaurant.service.domain.event;

import com.dev.food.ordering.system.domain.event.DomainEvent;
import com.dev.food.ordering.system.domain.valueobject.RestaurantId;
import com.dev.food.ordering.system.restaurant.service.domain.entity.OrderApproval;
import lombok.AllArgsConstructor;

import java.time.ZonedDateTime;
import java.util.List;

@AllArgsConstructor
public abstract class OrderApprovalEvent implements DomainEvent<OrderApproval> {
    private final OrderApproval orderApproval;
    private final RestaurantId restaurantId;
    private final List<String> failureMessages;
    private final ZonedDateTime createdAt;

    public OrderApproval getOrderApproval() {
        return orderApproval;
    }

    public RestaurantId getRestaurantId() {
        return restaurantId;
    }

    public List<String> getFailureMessages() {
        return failureMessages;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }
}
