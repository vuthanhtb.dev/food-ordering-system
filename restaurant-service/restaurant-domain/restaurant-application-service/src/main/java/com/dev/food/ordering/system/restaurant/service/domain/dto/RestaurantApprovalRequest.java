package com.dev.food.ordering.system.restaurant.service.domain.dto;

import com.dev.food.ordering.system.domain.valueobject.RestaurantOrderStatus;
import com.dev.food.ordering.system.restaurant.service.domain.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class RestaurantApprovalRequest {
    private String id;
    private String sagaId;
    private String restaurantId;
    private String orderId;
    private RestaurantOrderStatus restaurantOrderStatus;
    private List<Product> products;
    private java.math.BigDecimal price;
    private java.time.Instant createdAt;
}
