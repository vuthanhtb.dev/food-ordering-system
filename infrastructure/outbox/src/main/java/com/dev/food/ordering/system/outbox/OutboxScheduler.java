package com.dev.food.ordering.system.outbox;

public interface OutboxScheduler {
    void processOutboxMessage();
}
