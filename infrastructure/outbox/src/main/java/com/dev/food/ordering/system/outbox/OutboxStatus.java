package com.dev.food.ordering.system.outbox;

public enum OutboxStatus {
    STARTED, COMPLETED, FAILED
}
