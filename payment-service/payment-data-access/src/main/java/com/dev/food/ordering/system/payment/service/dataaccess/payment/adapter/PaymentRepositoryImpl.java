package com.dev.food.ordering.system.payment.service.dataaccess.payment.adapter;

import com.dev.food.ordering.system.payment.service.dataaccess.payment.mapper.PaymentDataAccessMapper;
import com.dev.food.ordering.system.payment.service.dataaccess.payment.repository.PaymentJpaRepository;
import com.dev.food.ordering.system.payment.service.domain.entity.Payment;
import com.dev.food.ordering.system.payment.service.domain.ports.output.repository.PaymentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@AllArgsConstructor
public class PaymentRepositoryImpl implements PaymentRepository {
    private final PaymentJpaRepository paymentJpaRepository;
    private final PaymentDataAccessMapper paymentDataAccessMapper;

    @Override
    public Payment save(Payment payment) {
        return paymentDataAccessMapper
                .paymentEntityToPayment(paymentJpaRepository
                        .save(paymentDataAccessMapper.paymentToPaymentEntity(payment)));
    }

    @Override
    public Optional<Payment> findByOrderId(UUID orderId) {
        return paymentJpaRepository.findByOrderId(orderId)
                .map(paymentDataAccessMapper::paymentEntityToPayment);
    }
}
