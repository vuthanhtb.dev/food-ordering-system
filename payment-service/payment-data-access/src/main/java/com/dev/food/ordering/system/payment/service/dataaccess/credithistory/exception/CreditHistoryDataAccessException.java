package com.dev.food.ordering.system.payment.service.dataaccess.credithistory.exception;

public class CreditHistoryDataAccessException extends RuntimeException {
    public CreditHistoryDataAccessException(String message) {
        super(message);
    }
}
